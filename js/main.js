var URL = "http://172.26.66.226:8080";
var URL_local = "http://localhost:8080"
var TIME = 150000000;
var LIMIT = 10;
var settings = {
    "async": true,
    "crossDomain": true,
    "url": URL_local+"/getTrends",
    "method": "GET",
    "dataType": "json",
    "headers": {
        "cache-control": "no-cache"
    }
}
var response = [
    {
        "id": "1",
        "location": "India"
    },
    {
        "id": "2",
        "location": "Canada"
    }
];


//=========================ACTUAL

tick();
// var ticker = setInterval(tick, TIME );

function tick() {
    $.ajax(settings).done(function (response) {
        $("#trending-list-ul")[0].innerHTML = "";
        
        var baseValue = response[0].bookingsCount;

        var finalTrends = "";
        for(var i =0 ; i<Math.min(response.length, LIMIT); i++) {
            finalTrends += "<li><a href=\"https://www.expedia.com/things-to-do/search?location="+ response[i].location+"\">"+response[i].location +"</a><div style=\"width: "+ Math.floor(response[i].bookingsCount/baseValue*100) +"%; height: 1px; margin-top: 3px; background-color: white;\"></div></li>";
        }
        $("#trending-list-ul")[0].innerHTML = finalTrends;
    });
}


//=========================MOCKED 


// tick();
// var ticker = setInterval(tick, TIME);

// function tick() {
      
//     $.ajax(settings).done(function (response) {
    
//     });

//     $("#trending-list-ul")[0].innerHTML = "";

//     var finalTrends = "";
//     for(var i =0 ; i<response.length; i++) {
//         finalTrends += "<li><a href=\"https://www.expedia.com/things-to-do/search?location="+ response[i].location+"\">"+response[i].location +"</a></li>";
//     }
//     $("#trending-list-ul")[0].innerHTML = finalTrends;

//     changeResponse();
// }

// function changeResponse() {

//     var c1 = Math.floor((Math.random() * 10));
//     var c2 = Math.floor((Math.random() * 10));
//     var c3 = Math.floor((Math.random() * 10));
//     var c4 = Math.floor((Math.random() * 10));

//     var temp = response[c1].location;
//     response[c1].location = response[c2].location;
//     response[c2].location = temp;

//     var temp1 = response[c3].location;
//     response[c3].location = response[c4].location;
//     response[c4].location = temp1;
// }





        
            
